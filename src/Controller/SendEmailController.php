<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SendEmailController extends AbstractController
{
    /**
     * @Route("/send/email", name="send_email")
     */
    public function index(\Swift_Mailer $mailer)
    {
       $name = "abo  el  yousr";
      $message = (new \Swift_Message('Hello Email'))
         ->setFrom('send@example.com')
         ->setTo('recipient@example.com')
         ->setBody(
             $this->renderView(
                 // templates/emails/registration.html.twig
                 'send_email/index.html.twig',
                 ['name' => $name]
             ),
             'text/html'
         )

         // you can remove the following code if you don't define a text version for your emails
         // ->addPart(
         //     $this->renderView(
         //         // templates/emails/registration.txt.twig
         //         'send_email/index.html.twig',
         //         ['name' => $name]
         //     ),
         //     'text/plain'
         // )
     ;

     $mailer->send($message);




        return $this->render('send_email/index.html.twig', [
            'name' => $name,
        ]);
    }
}
