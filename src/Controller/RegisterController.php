<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {

      if ($request->isMethod('POST')) {
         $user = new User();
         $user->setUsername($request->request->get('username'));

         $user->setEmail($request->request->get('email'));

         $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
         $em = $this->getDoctrine()->getManager();
         $em->persist($user);
         $em->flush();
         return $this->redirectToRoute('app_login');
     }

     return $this->render('register/index.html.twig');

    }
}
