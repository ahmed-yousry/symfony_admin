<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/reset/password/{token}", name="reset_password")
     */



    public function index(Request $request,string $token, UserPasswordEncoderInterface $passwordEncoder)
    {

      if ($request->isMethod('POST')) {
           $entityManager = $this->getDoctrine()->getManager();

            $Get_user = $entityManager->getRepository(User::class)->findOneByResetToken($token);
               $user = new User();
               //

            /* @var $user User */

            if ($Get_user === null) {
                $this->addFlash('danger', 'Token Inconnu');
                return $this->redirectToRoute('app_login');
            }

// dump($entityManager->setResetToken());
// die();
          $ResetToken=  $user->setResetToken(null);

          $password=  $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));


        $update_password = $entityManager->getRepository(User::class)->EditResetToken($password,$ResetToken);

        // dump($x);
        // die();

            $this->addFlash(
            'info',
            'Added successfully'
          );

            return $this->redirectToRoute('app_login');
        }else {

            return $this->render('reset_password/index.html.twig', ['token' => $token]);
        }



    }
}
