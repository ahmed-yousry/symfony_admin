<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Entity\User;

class ForgotPasswordController extends AbstractController
{
    /**
     * @Route("/forgot/password", name="forgot_password")
     */
    public function index(Request $request,UserPasswordEncoderInterface $encoder,
    \Swift_Mailer $mailer)
    {

      if ($request->isMethod('POST')) {


           $email = $request->request->get('email');

           $entityManager = $this->getDoctrine()->getManager();
           $user = $entityManager->getRepository(User::class)->findOneByEmail($email);
           /* @var $user User */

           if ($user === null) {
               $this->addFlash('danger', 'Email Inconnu');
               return $this->redirectToRoute('homepage');
           }

           $token = $user->generateToken();

           try{
               $user->setResetToken($token);
               $entityManager->flush();
           } catch (\Exception $e) {
               $this->addFlash('warning', $e->getMessage());
               return $this->redirectToRoute('homepage');
           }

           $url = $this->generateUrl('reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);

           $message = (new \Swift_Message('Forgot Password'))
               ->setFrom('g.ponty@dev-web.io')
               ->setTo($user->getEmail())
               ->setBody(
                   "blablabla voici le token pour reseter votre mot de passe : " . $url,
                   'text/html'
               );

           $mailer->send($message);

           $this->addFlash('notice', 'Mail envoyé');

           return $this->redirectToRoute('app_login');
       }


        return $this->render('forgot_password/index.html.twig', [
            'controller_name' => 'ForgotPasswordController',
        ]);
    }













}
