<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*

    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneByResetToken($value)
    {

      return   $this->createQueryBuilder('user')
        ->andWhere('user.resetToken = :val')
        ->setParameter('val', $value)
        ->orderBy('user.resetToken', 'ASC')
        ->setMaxResults(10)
        ->getQuery()
        ->getResult()
    ;

    }

    public function EditResetToken($password,$ResetToken)
    {
       // $queryBuilder =  $this->createQueryBuilder('u');



       $connection =  $this->getEntityManager()->getConnection();
       $sql="UPDATE `user` SET password`='$password',`reset_token`='$ResetToken' WHERE 1";

           $statment = $connection->prepare($sql);
           $statment->execute(['username'=>'admin']);
           return  $statment->fetchAll();
       // $qb = $queryBuilder->createQueryBuilder();
       // $qb->update('User', 'u')
       // ->set("u.field", "new value")
       // ->where("u.field = :oldvalue")
       // ->setParameter("oldvalue", "old value");

// $queryBuilder
//     ->update('user', 'u')
//     ->set('u.password',$password)
//     ->set('u.last_login', $ResetToken)
//     // ->setParameter(0, $userInputLastLogin)
// ;
   // $qb->select('p.title','p.description');
   //
   // dump($qb->getQuery()->getResult());
   // die();
  // dump($queryBuilder->getQuery()->getResult());
    }

}
