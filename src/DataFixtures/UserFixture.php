<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
class UserFixture extends Fixture
{
  private $encoder;
  public function __construct(UserPasswordEncoderInterface $encoder)
  {
            $this->encoder = $encoder;
  }
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('admin@admin.com');
        $user->setPassword(
          $this->encoder->encodePassword($user ,'4562008')
        );
        $user->setRoles('ROLE_USER');
        $generate_token = $user->generateToken();
        $user->setResetToken($generate_token);
        
        $manager->persist($user);

        $manager->flush();
    }
}
